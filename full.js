'use strict';

const exercise1 = require('./exercise1');
const exercise2 = require('./exercise2');

const MAX_NUMBER_TO_PRINT = 100;
const DATASET_SECOND_EXERCISE = [
    {
        country: "US",
        languages: [ "en" ]
    },
    {
        country: "BE",
        languages: [ "nl", "fr", "de" ]
    },
    {
        country: "NL",
        languages: [ "nl", "fy" ]
    },
    {
        country: "DE",
        languages: [ "de" ]
    },
    {
        country: "ES",
        languages: [ "es" ]
    }
];

// Exercise 1
// Write or describe an algorithm that prints the whole integer numbers to the console, start from the number 1, and
// print all numbers going up to the number 100. However, when the number is divisible by 3, do not print the number,
// but print the word 'Visual'. If the number is divisible by 5, do not print the number, but print 'Nuts'. And for
// all numbers divisible by both (eg: the number 15) the same, but print 'Visual Nuts'.

let resultArray = exercise1.printNumbersNotDivisableByThreeOrFive(MAX_NUMBER_TO_PRINT);
resultArray.forEach(el => console.log(el));

// Exercise 2
// Write a function in Javascript that:
// - returns the number of countries in the world
let numberOfCountriesInTheWorld = exercise2.howManyCountriesInTheWorld(DATASET_SECOND_EXERCISE);
console.log(`There are ${numberOfCountriesInTheWorld} countries in the world.`)

// - finds the country with the most official languages,where they officially speak German (de).
let countryWithTheMostOfficialLanguagesThatSpeakGerman = exercise2.countryWithTheMostOfficialLanguagesThatSpeakAGivenLanguage(DATASET_SECOND_EXERCISE, 'de');
console.log(`The country with the most official languages, where the speak German is ${countryWithTheMostOfficialLanguagesThatSpeakGerman.country}`);

// - that counts all the official languages spoken in the listed countries.
let officialLanguagesCounter = exercise2.officialLanguagesCounterByCountry(DATASET_SECOND_EXERCISE);
officialLanguagesCounter.forEach(el => console.log(`The country ${el.country} has ${el.counter} official languages`));

// - to find the country with the highest number of official languages
let countryWithTheMostOfficialLanguages = exercise2.getCountryWithTheHighestNumberOfOfficialLanguages(DATASET_SECOND_EXERCISE);
console.log(`The country with the highest number of official languages is ${countryWithTheMostOfficialLanguages.country} with ${countryWithTheMostOfficialLanguages.languages.length} languages.`)

// - to find the most common official language(s), of all countries
let mostCommonOfficialLanguages = exercise2.getMostCommonOfficialLanguages(DATASET_SECOND_EXERCISE);
console.log(`The most common official language(s) is/are ${mostCommonOfficialLanguages.join(', ')}.`)