'use strict';

exports.howManyCountriesInTheWorld = (data) => {
    return data.length;
}

exports.countryWithTheMostOfficialLanguagesThatSpeakAGivenLanguage = (data, language) => {
    return data.filter((element) => {
        return element.languages.indexOf(language) >= 0;
    }).reduce((prev, curr) => {
        return prev.languages.length > curr.languages.length ? prev : curr;
    })
}

exports.officialLanguagesCounterByCountry = (data) => {
    return data.map(element => {
        return {...element, counter: element.languages.length};
    });
}

exports.getCountryWithTheHighestNumberOfOfficialLanguages = (data) => {
    return data.reduce((prev, curr) => {
        return prev.languages.length > curr.languages.length ? prev : curr;
    });
}

exports.getMostCommonOfficialLanguages = (data) => {
    const count = {};
    for (const element of data) {
        for (const language of element.languages) {
            count[language] = (count[language] || 0 ) + 1; // if no language in object, create and set to zero and increment counter
        }
    }

    return Object.keys(count).reduce((acc, cur, idx) => { // iterate over languages
        if (!idx || count[cur] > count[acc[0]]) { // if first iteration or current count greater than accumulator's count
            return [cur]; // returns first iteration or resets the accumulator to hold the greater value
        }
        if (count[cur] === count[acc[0]]) { // if current count equals the greater so far
            acc.push(cur); // adds current to accumulator (it could have more than one country with the same amount of languages
        }
        return acc;
    }, []);
}
