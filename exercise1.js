'use strict';


const VISUAL = "Visual";
const NUTS = "Nuts";
const VISUALNUTS = "Visual Nuts";


exports.printNumbersNotDivisableByThreeOrFive = (upperLimit) => {
    console.log("== Exercise 1 ==");
    let acc = [];
    for (let i = 1; i <= upperLimit; i++) {
        if (i % 3 === 0 && i % 5 === 0) {
            acc.push(VISUALNUTS);
            continue;
        }
        if (i % 3 === 0) {
            acc.push(VISUAL);
            continue;
        }
        if (i % 5 === 0) {
            acc.push(NUTS);
            continue;
        }
        acc.push(i);
    }
    return acc;
}
